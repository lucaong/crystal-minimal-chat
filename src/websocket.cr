require "./websocket/*"
require "http"
require "json"

module Websocket
  class Chatroom
    getter :handler

    def initialize(@name = "unnamed")
      @sessions  = [] of HTTP::WebSocketHandler::WebSocketSession
      @envelopes = [] of Hash(Symbol, String | UInt64)
      @handler   = create_handler!
    end

    def open!(port = 8080)
      server = HTTP::Server.new(port, [handler, HTTP::StaticFileHandler.new("public")])
      puts "Chatroom #{@name} opened on port #{port}"
      server.listen
    end

    private def create_handler!
      HTTP::WebSocketHandler.new do |session|
        puts "#{session.object_id} joined"
        @sessions.push(session)
        @envelopes.each do |envelope|
          session.send(envelope.to_json)
        end

        session.on_message do |message|
          envelope = wrap(message, session.object_id)
          puts "#{envelope[:sender]} sent: #{envelope[:message]}"
          @envelopes << envelope
          @sessions.each do |session|
            session.send(envelope.to_json)
          end
        end

        session.on_close do |message|
          puts "#{session} left"
          @sessions.reject! { |el| el == session }
        end
      end
    end

    private def wrap(message, sender)
      { message: message, sender: sender }
    end
  end
end

chatroom = Websocket::Chatroom.new
chatroom.open!
